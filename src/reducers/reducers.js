import { combineReducers } from 'redux';
import { UserStartLatLngReducer, 
    UserEndLatLngReducer,
    routeLatLngTokenReducer,
    shortestLatLngReducer
 } from '../reducers/MapReducer';
const rootReducer = combineReducers({
    UserStartLatLng: UserStartLatLngReducer,
    UserEndLatLng: UserEndLatLngReducer,
    routeLatLngToken: routeLatLngTokenReducer,
    shortestLatLng: shortestLatLngReducer
});

export default rootReducer;