import { GET_USER_START_LAT_LONG, 
    GET_USER_END_LAT_LONG, 
    SUBMIT_USER_LAT_LNG,GET_USER_SHORTEST_PATH 
} from '../actions/types';

export function UserStartLatLngReducer(state=null, action){
    switch(action.type){
        case GET_USER_START_LAT_LONG:
            return action.payload
        default: return state;
    }
}

const defaultState = {
    destinations: []
};

export function UserEndLatLngReducer(state=defaultState, action){    
    switch(action.type){
        case GET_USER_END_LAT_LONG:
            const destinations = [...state.destinations];
            destinations.push(action.payload);
            return {
                    destinations
            }
        default: return state;
    }
}

export function routeLatLngTokenReducer(state=null, action){
    switch(action.type){
        case SUBMIT_USER_LAT_LNG:
            if(action.payload.data && action.payload.data.token){
                return {
                    message: 'success',
                    token: action.payload.data.token
                }
            }
        default: return state;
    }
}

export function shortestLatLngReducer(state=null, action){
    switch(action.type){
        case GET_USER_SHORTEST_PATH:
            return action.payload.data
        default: return state;
    }
}

