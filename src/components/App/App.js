import React, { Component } from 'react';
import './App.css';
import Map from '../Map/map';
import Sidebar from '../Sidebar/sidebar';
import { Grid, Col, Row } from 'react-bootstrap';
class App extends Component {
  render() {
    return (
      <Grid fluid={true} className="App">
        <Row className="app-row">
          <Col xs={5}>
            <Sidebar />
          </Col>
          <Col xs={7}>
            <Map isMarkerShown/>
          </Col>
        </Row>
          
      </Grid>
    );
  }
}

export default App;
