import React, { Component } from 'react';
import './sidebar.css';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
// import { Field, reduxForm } from 'redux-form';
import { Col,Button  } from 'react-bootstrap';
import { getUserOriginLatLong, getUserDestinationLatLong,submitUserLatLng } from '../../actions/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
 
class Sidebar extends Component {
    constructor(props) {
        super(props);  
        this.state = { 
            originAddress: '',
            destinationAddress: [],
            destinations: 1
        }
        this.renderFields = this.renderFields.bind(this);
    }
    
    handleChange = (address,type,index) => {
        if(type === 'start'){
            this.setState({originAddress: address })
        }
        else{
            const prevdestinations = [...this.state.destinationAddress];
            prevdestinations[index] = address;
            this.setState({destinationAddress: prevdestinations})
        }
    }
    submitUserLatLngBtnClicked = () =>{
        if(this.props.UserEndLatLng.destinations.length && this.props.UserStartLatLng){
            const destinations = this.props.UserEndLatLng.destinations.map((obj) => [obj.lat, obj.lng]);
            this.props.submitUserLatLng([[this.props.UserStartLatLng.lat, this.props.UserStartLatLng.lng], ...destinations]);
        }
        else{
            alert("please enter locations")
        }
    }

    handleSelect = (address,type) => {
        geocodeByAddress(address)
        .then(results => getLatLng(results[0]))
        .then(latLng => {
            if(type === 'start'){
                this.props.getUserOriginLatLong(latLng);
            }
            else{
                this.props.getUserDestinationLatLong(latLng)
            }
        })
        .catch(error => console.log('Error', error))
    }
    renderFields(type, index){
        return (
            <PlacesAutocomplete
                key={index}
                value={type === 'start' ? this.state.originAddress : this.state.destinationAddress[index] || ''}
                onChange={(address) =>this.handleChange(address,type, index)}
                onSelect={(address) =>this.handleSelect(address,type)}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                <div>
                    <input
                    {...getInputProps({
                        placeholder: type === 'start' ? 'Enter start location' : 'Enter end location' ,
                        className: 'form-control location-search-input'
                    })}
                    />

                    <div className="autocomplete-dropdown-container">
                    {suggestions.map(suggestion => {
                        const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';
                        // inline style for demonstration purpose
                        const style = suggestion.active
                                    ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                    : { backgroundColor: '#ffffff', cursor: 'pointer' };
                        return (
                        <div {...getSuggestionItemProps(suggestion, { className, style })}>
                            <span>{suggestion.description}</span>
                        </div>
                        )
                    })}
                    </div>
                </div>
                )}
            </PlacesAutocomplete>
        )
    }

    addDestination = () => {
        this.setState((prevState) => ({
            destinations: prevState.destinations + 1
        }));
    }

    render() {
        return (
        <div className="Sidebar">
            <Col xs={6}>
                {this.renderFields('start', 999999)}
            </Col>
            <Col xs={6}>
                {
                    Object.keys(Array(this.state.destinations).fill(0)).map((val) => this.renderFields('end', val))
                }
                <Col xs={12} style={{
                    padding: '0',
                    marginTop: '10px'       
                }}>
                    <Button onClick={this.addDestination}>Add new Destination</Button>                
                </Col>
            </Col>
          
            <Col xs={12}>
                <Button onClick={this.submitUserLatLngBtnClicked}>Submit</Button>
            </Col>
        </div>
        );
    }
}

function mapStateToProps(state){
    return {
        UserEndLatLng: state.UserEndLatLng,
        UserStartLatLng: state.UserStartLatLng
    }
}

function mapDistpatchToProps(dispatch){
    return bindActionCreators({
        getUserOriginLatLong,
        getUserDestinationLatLong,
        submitUserLatLng
    }, dispatch)
}

export default connect(mapStateToProps,mapDistpatchToProps)(Sidebar);
