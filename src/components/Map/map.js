/*global google*/
import React, { Component } from 'react';
import './map.css';
import { withGoogleMap, GoogleMap,DirectionsRenderer } from 'react-google-maps';
import { connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import {getUserShortestPathLatLng} from '../../actions/actions'
class Map extends Component {
    constructor(props){
        super(props)
        this.state ={
            directions: [],
            DirectionsService: new google.maps.DirectionsService()
        }
        this.GoogleDirectionsService = this.GoogleDirectionsService.bind(this)
    }
    componentWillReceiveProps(nextprops){
        if(nextprops.routeLatLngToken && nextprops.routeLatLngToken !== this.props.routeLatLngToken){
            nextprops.getUserShortestPathLatLng(nextprops.routeLatLngToken)
        }
        else if(nextprops.routeLatLngToken && nextprops.routeLatLngToken.message === 'error'){
            alert("Internal Server Error");
        }
        if(nextprops.shortestLatLng){
            if(nextprops.shortestLatLng.status === 'success'){
                const arr = nextprops.shortestLatLng.path;
                const arrLength = arr.length;
                arr.forEach((location, index) => {
                    if(index + 1 < arrLength){
                        const nextLocation = arr[index+1];
                        return this.GoogleDirectionsService(location, nextLocation,index);
                    }
                });
            }
            else{
                alert(nextprops.shortestLatLng.status)
            }
        }
        else{
            this.setState({
                directions: [],
            });
        }

    }
    GoogleDirectionsService(start,end, index){
        this.state.DirectionsService.route({
            origin: new google.maps.LatLng(start[0],start[1]),
            destination: new google.maps.LatLng(end[0],end[1]),
            travelMode: google.maps.TravelMode.DRIVING,
        }, (result, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
                // console.log(result)
                const direction = (<DirectionsRenderer key={index} directions={result} />);
                this.setState((prevState) => ({
                    directions: [...prevState.directions, direction],
                }));
            } 
            else {
                console.error(`error fetching directions ${result}`);
            }
        });
    }
    render() {
    const GoogleMapComponent = withGoogleMap(props => (
        <GoogleMap
            defaultZoom={7}
            defaultCenter={new google.maps.LatLng(this.props.UserStartLatLng ? this.props.UserStartLatLng :41.8507300, -87.6512600)}
        >
            {this.state.directions}
      </GoogleMap>
     ));
    return (
      <div className="map">
      <GoogleMapComponent
          containerElement={ <div style={{ height: `500px`, width: '100%' }} /> }
          mapElement={ <div style={{ height: `100%` }} /> }
        />
      </div>
    );
  }
}

function mapStateToProps(state){
    return{
        UserEndLatLng: state.UserEndLatLng,
        UserStartLatLng: state.UserStartLatLng,
        routeLatLngToken: state.routeLatLngToken,
        shortestLatLng: state.shortestLatLng
    }
}

function mapDispatchToProps(dispatch){
   return bindActionCreators({
        getUserShortestPathLatLng
    },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(Map);
