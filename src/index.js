import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App/App';
import registerServiceWorker from './registerServiceWorker';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducers from './reducers/reducers';
import promiseMiddleware from 'redux-promise';

const store = createStore(reducers,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),applyMiddleware(promiseMiddleware));
ReactDOM.render(
    <Provider store={store}>
          <App />
    </Provider>,
   document.getElementById('root'));
registerServiceWorker();
