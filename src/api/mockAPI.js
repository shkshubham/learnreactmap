import axios from 'axios';

export function submitUserLatLngAPI(data){
    return axios.post('http://localhost:8080/route',data)
}

export function fetchUserShortestLatLngAPI(token){
    return axios.get(`http://localhost:8080/route/${token.token}`)
}