import { GET_USER_START_LAT_LONG, 
    GET_USER_END_LAT_LONG,
    SUBMIT_USER_LAT_LNG,
    GET_USER_SHORTEST_PATH, 
} from './types';
import { submitUserLatLngAPI,fetchUserShortestLatLngAPI } from '../api/mockAPI';

export function getUserOriginLatLong(LatLongObj){
    return {
        type: GET_USER_START_LAT_LONG,
        payload: LatLongObj
    }
}

export function getUserDestinationLatLong(LatLongObj){
    return {
        type: GET_USER_END_LAT_LONG,
        payload: LatLongObj
    }
}


export function submitUserLatLng(data){
    let response = submitUserLatLngAPI(data)
    return {
        type: SUBMIT_USER_LAT_LNG,
        payload: response
    }
}

export function getUserShortestPathLatLng(token){
    let response = fetchUserShortestLatLngAPI(token)
    return {
        type: GET_USER_SHORTEST_PATH,
        payload: response
    }
}